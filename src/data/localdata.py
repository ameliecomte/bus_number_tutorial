"""
Custom dataset processing/generation functions should be added to this file
"""

import pathlib
from .utils import read_space_delimited
import numpy as np

__all__ = [
    'process_lvq_pak',
    'process_fmnist'
]


def process_lvq_pak(*, unpack_dir, kind='all', extract_dir='lvq_pak-3.1', metadata=None):
    """
    Parse LVQ-PAK datafiles into usable numpy arrays
    
    Parameters
    ----------
    unpack_dir: path
        path to unpacked tarfile
    extract_dir: string
        name of directory in the unpacked tarfile containing
        the raw data files
    kind: {'train', 'test', 'all'}
    
    
    Returns
    -------
    A tuple: 
       (data, target, additional_metadata)
    
    """
    if metadata is None:
        metadata = {}

    if unpack_dir:
        unpack_dir = pathlib.Path(unpack_dir)

    data_dir = unpack_dir / extract_dir

    if kind == 'train':
        data, target, metadata = read_space_delimited(data_dir / 'ex1.dat',
                                                      skiprows=[0,1],
                                                      metadata=metadata)
    elif kind == 'test':
        data, target, metadata = read_space_delimited(data_dir / 'ex2.dat',
                                                      skiprows=[0],
                                                      metadata=metadata)
    elif kind == 'all':
        data1, target1, metadata = read_space_delimited(data_dir / 'ex1.dat', skiprows=[0,1],
                                                        metadata=metadata)
        data2, target2, metadata = read_space_delimited(data_dir / 'ex2.dat', skiprows=[0],
                                                        metadata=metadata)
        data = np.vstack((data1, data2))
        target = np.append(target1, target2)
    else:
        raise Exception(f'Unknown kind: {kind}')

    return data, target, metadata

def read_fmnist(data_path, kind='train', metadata=None):
    """
    Helper function to read fmnist data files.
    
    Parameters
    ----------
    data_path: path
        base directory to look for the files in
    kind: one of 'train' and 'test'
        whether to parse the training or test datasets
    metadata: dict
        metadata to add to the process
    
    Returns
    -------
    (data, target, metadata)
    """
    data_path = pathlib.Path(data_path)
    
    if kind == 'train':
        name_kind = kind
    elif kind == 'test':
        name_kind = 't10k'
    else:
        raise ValueError(f"Unknown kind:{kind}")

    # parsing labels aka. target
    with open(data_path / f'{name_kind}-labels-idx1-ubyte', 'rb') as labels:
        target = np.frombuffer(labels.read(), dtype=np.uint8, offset=8)
        
    # parsing images aka. data
    with open(data_path / f'{name_kind}-images-idx3-ubyte', 'rb')as images:
        data = np.frombuffer(images.read(), dtype=np.uint8, offset=16).reshape(len(target), 784)
        
    return data, target, metadata

def process_fmnist(*, unpack_dir, kind='all', extract_dir=None, metadata=None):
    """
    Load the F-MNIST dataset 

    Parameters
    ----------
    unpack_dir: path
        path to unpacked tarfile
    kind: {'train', 'test', 'all'}
        Dataset comes pre-split into training and test data.
        Indicates which dataset to load
    metadata: dict
        Additional metadata fields will be added to this dict.
        'kind': value of `kind` used to generate a subset of the data
    '''
    
    Returns
    -------
    A tuple: 
       (data, target, additional_metadata)
    
    """
    if metadata is None:
        metadata = {}

    if unpack_dir:
        unpack_dir = pathlib.Path(unpack_dir)
    if extract_dir is not None:
        data_dir = unpack_dir / extract_dir
    else:
        data_dir = unpack_dir

    if kind == 'train':
        data, target, metadata = read_fmnist(data_dir, kind='train', metadata=metadata)
    elif kind == 'test':
        data, target, metadata = read_fmnist(data_dir, kind='test', metadata=metadata)
    elif kind == 'all':
        data1, target1, metadata = read_fmnist(data_dir, kind='train', metadata=metadata)
        data2, target2, metadata = read_fmnist(data_dir, kind='test', metadata=metadata)
        data = np.vstack((data1, data2))
        target = np.append(target1, target2)
    else:
        raise Exception(f'Unknown kind: {kind}')

    return data, target, metadata
